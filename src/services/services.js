import { Http } from "./http/http.service";
import { Messages } from "./messages/messages.service";
import { Users } from "./users/users.service";

const http = new Http();
const messages = new Messages({
  baseUrl: "http://localhost:3001",
  http,
});
const users = new Users({
  baseUrl: "http://localhost:3001",
  http,
}); 

export { http, messages, users };