import { createReducer } from "@reduxjs/toolkit";
import { DataStatus } from "../../common/enums/enums";
import { fetchMessage } from "./actions";

const initialState = {
    message: null,
    status: DataStatus.IDLE,
}

const reducer = createReducer(initialState, (builder) => {
    builder.addCase(fetchMessage.pending, (state) => {
        state.status = DataStatus.PENDING;
    });
    builder.addCase(fetchMessage.fulfilled, (state, { payload }) => {
        const { message } = payload;

        state.message = message;
        state.status = DataStatus.SUCCESS;
    });
});

export { reducer };