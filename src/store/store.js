import { configureStore } from "@reduxjs/toolkit";
import { messages as messagesService } from '../services/services';
import { users as usersService } from '../services/services';
import { messages } from './root-reducer';
import { message } from './root-reducer';
import  { users } from './root-reducer';
import { user } from './root-reducer';

const store = configureStore({
    reducer: {
        messages,
        users,
        message,
        user,
    },
    middleware: (getDefaultMiddleware) => {
        return getDefaultMiddleware({
            thunk: {
                extraArgument: {
                    messagesService,
                    usersService
                },
            },
        });
    },
});

export { store };