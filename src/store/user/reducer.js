import { createReducer } from "@reduxjs/toolkit";
import { DataStatus } from "../../common/enums/enums";
import { fetchUser } from "./actions";

const initialState = {
    user: null,
    status: DataStatus.IDLE,
}

const reducer = createReducer(initialState, (builder) => {
    builder.addCase(fetchUser.pending, (state) => {
        state.status = DataStatus.PENDING;
    });
    builder.addCase(fetchUser.fulfilled, (state, { payload }) => {
        const { user } = payload;

        state.user = user;
        state.status = DataStatus.SUCCESS;
    });
});

export { reducer };