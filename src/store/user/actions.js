import { createAsyncThunk } from "@reduxjs/toolkit";
import { ActionType } from './common';

const fetchUser = createAsyncThunk(ActionType.FETCH_USER, async (id, { extra }) => ({
    user: await extra.usersService.getOne (id),
}));

export { fetchUser };