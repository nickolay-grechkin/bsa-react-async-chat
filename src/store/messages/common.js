const ActionType = {
    FETCH_MESSAGES: 'messages/fetch-messages',
    ADD: 'messages/add',
    UPDATE: 'messages/update',
    LIKE: 'messages/like',
    DELETE: 'messages/delete',
};

export { ActionType };