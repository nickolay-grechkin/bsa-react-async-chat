import { createAsyncThunk, createAction } from "@reduxjs/toolkit";
import { ActionType } from './common';

const fetchMessages = createAsyncThunk(ActionType.FETCH_MESSAGES, async (_args, {extra}) => ({
    messages: await extra.messagesService.getAll(),
}));

const addMessage = createAsyncThunk(ActionType.ADD, async (payload, { extra }) => ({
    message: await extra.messagesService.create(payload),
}))

const updateMessage = createAsyncThunk(ActionType.UPDATE, async (payload, { extra }) => ({
    message: await extra.messagesService.update(payload),
}));

const likeMessage = createAction(ActionType.LIKE, function prepare(id) {
    return {
        payload: {
          id: id,
        },
      }
});

const deleteMessage = createAsyncThunk(ActionType.DELETE, async (message, { extra }) => {
    await extra.messagesService.delete(message.id);
    return {
        message,
    };
});

export { fetchMessages, addMessage, updateMessage, likeMessage, deleteMessage };