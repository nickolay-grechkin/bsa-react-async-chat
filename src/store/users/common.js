const ActionType = {
    FETCH_USERS: 'users/fetch-messages',
    ADD: 'users/add',
    UPDATE: 'users/update',
    DELETE: 'users/delete',
};

export { ActionType };