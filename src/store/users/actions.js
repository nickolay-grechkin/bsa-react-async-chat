import { createAsyncThunk } from "@reduxjs/toolkit";
import { ActionType } from './common';

const fetchUsers = createAsyncThunk(ActionType.FETCH_USERS, async (_args, {extra}) => ({
    users: await extra.usersService.getAll(),
}));

const addUser = createAsyncThunk(ActionType.ADD, async (payload, { extra }) => ({
    user: await extra.usersService.create(payload),
}))

const updateUser = createAsyncThunk(ActionType.UPDATE, async (payload, { extra }) => ({
    user: await extra.usersService.update(payload),
}));

const deleteUser = createAsyncThunk(ActionType.DELETE, async (user, { extra }) => {
    await extra.usersService.delete(user.id);
    return {
        user,
    };
});

export { fetchUsers, addUser, updateUser, deleteUser };