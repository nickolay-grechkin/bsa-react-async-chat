const MessageKey = {
    ID: 'id',
    USERIDENT: 'userIdent',
    AVATAR: 'avatar',
    USER: 'user',
    TEXT: 'text',
    CREATEDAT:'createdAt',
    EDITEDAT:"editedAt",
}

export { MessageKey };