const UserKey = {
    ID: 'id',
    NAME: 'name',
    SURNAME: 'surname',
    EMAIL: 'email',
    PASSWORD: 'password',
  };
  
  export { UserKey };