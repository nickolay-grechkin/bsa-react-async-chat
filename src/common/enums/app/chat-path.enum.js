const ChatPath = {
    ROOT: '/',
    LOGIN: '/login',
    EDIT: '/edit',
    EDIT_$ID: '/edit/:id',
    EDITUSER:'/edit-user',
    EDITUSER_$ID:'/edit-user/:id',
    USERS: '/users',
    MESSAGES: '/messages',
    ANY: '*',
};

export { ChatPath };