const PageNotFound = () => (
    <section className="page-notfound">
      <h2 className="sr-only">Page not found</h2>
    </section>
);
  
export default PageNotFound;