import React, { useEffect, useState } from 'react';
import { users as usersActionCreator} from '../../store//actions';
import { useDispatch, useSelector } from 'react-redux';
import { ChatPath } from '../../common/enums/enums';
import { useHistory } from "react-router-dom";
import { Avatar, Button, CssBaseline, TextField, 
    LockOutlinedIcon, Typography, makeStyles, Container } from './material-components';
import Loader from '../common/loader/loader';
import './style.css';

const useStyles = makeStyles((theme) => ({
    paper: {
        marginTop: theme.spacing(8),
        display: 'flex',
        flexDirection: 'column',
        alignItems: 'center',
    },
    avatar: {
        margin: theme.spacing(1),
        backgroundColor: theme.palette.secondary.main,
    },
    form: {
        width: '100%',
        marginTop: theme.spacing(1),
    },
    submit: {
        margin: theme.spacing(3, 0, 2),
        textDecoration: 'none',
    },
}));

export default function SignIn() {
    const classes = useStyles();
    const [currentUser, setCurrentUser] = useState({email:'', password:'',});
    const { users, status } = useSelector(({ users }) => ({
        users: users.users,
        status: users.status,
    }));
    const dispatch = useDispatch();
    const history = useHistory();

    useEffect(() => {
        dispatch(usersActionCreator.fetchUsers());
    }, [dispatch]);

    const handleChange = ({ target }) => {
        const { name, value } = target;
        setCurrentUser({ ...currentUser, [name]: value });
    };

    const handleClick = () => {
        const correct = users.filter((it) => it.email === currentUser.email && it.password === currentUser.password);
        if (correct.length === 0) {
            history.push(ChatPath.LOGIN);
        }
        else if (correct[0].email === "admin") {
            history.push(ChatPath.USERS);
        }
        else {
            history.push(ChatPath.MESSAGES);
        }
    };

    if (status === "pending") {
        return <Loader />;
    }

    return (
        <Container component="main" maxWidth="xs">
        <CssBaseline />
        <div className={classes.paper}>
            <Avatar className={classes.avatar}>
                <LockOutlinedIcon />
            </Avatar>
            <Typography component="h1" variant="h5">
                Sign in
            </Typography>
            <form className={classes.form} noValidate>
                <TextField
                    variant="outlined"
                    margin="normal"
                    required
                    fullWidth
                    id="email"
                    label="Email Address"
                    name="email"
                    onChange={handleChange}
                    autoComplete="email"
                    autoFocus
                />
                <TextField
                    variant="outlined"
                    margin="normal"
                    required
                    fullWidth
                    name="password"
                    label="Password"
                    type="password"
                    id="password"
                    autoComplete="current-password"
                    onChange={handleChange}
                />
                <Button
                    fullWidth
                    variant="contained"
                    color="primary"
                    onClick={handleClick}
                >
                    <p className="login-btn"> Sign In </p>
                </Button>
            </form>
        </div>
        </Container>
    );
}