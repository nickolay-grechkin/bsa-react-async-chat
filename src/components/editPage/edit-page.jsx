import { useCallback, useEffect, useState } from 'react';
import { getLastPath } from '../../helpers/location/get-last-path/get-last-path';
import Button from '@material-ui/core/Button';
import { message as messageActionCreator } from '../../store/actions';
import { messages as messagesActionCreator } from '../../store/actions'
import { useDispatch, useSelector } from 'react-redux';
import Loader from '../common/loader/loader';
import { DataStatus } from '../../common/enums/enums';
import { ChatPath } from '../../common/enums/enums';
import { Link } from 'react-router-dom';
import TextareaAutosize from '@material-ui/core/TextareaAutosize';
import './style.css';

const EditPage = () => {
    const [newText, setNewText] = useState(" ");
    
    const { message, status } = useSelector(({message}) => ({
        message: message.message,
        status: message.status,
    }));

    const dispatch = useDispatch();
    
    useEffect(() => {
        const id = getLastPath(window.location.pathname);
        dispatch(messageActionCreator.fetchMessage(id));
    }, [dispatch]);
    
    const handleMessageUpdate =  useCallback((message, newText) => {
        const newMessage = {
            id:message.id,
            userIdent:message.userIdent,
            avatar:message.avatar,
            user:message.user,
            text:newText,
            createdAt:message.createdAt,
            editedAt:" ",
        }
        dispatch(messagesActionCreator.updateMessage(newMessage));
    }, [dispatch]);

    const onChange = (e) => {
        setNewText(e.target.value);  
    };

    if (status !== DataStatus.SUCCESS) {
        return <Loader />;
    }
    
    return (
        <div>
            <h1 className="edit-label">Edit message</h1>
            <TextareaAutosize  
                className="message-input-edit"
                placeholder="Input message"
                inputProps={{ 'aria-label': 'search google maps' }}
                onChange = {onChange}
            />
            <Link className="link-close" to={ChatPath.MESSAGES}>
                <Button className="button-close" >
                    Close
                </Button>
            </Link>
            <Link className="link-update" to={ChatPath.MESSAGES}>
                <Button onClick={() => handleMessageUpdate(message, newText)}>
                    Update
                </Button>
            </Link> 
        </div>
    );
} 

export default EditPage;