import React from 'react';
import { useState } from 'react';
import { Paper, IconButton, DirectionsIcon, InputBase } from './material-components';
import './style.css';

const MessageInput = ({message, onSave}) => {
    const [currentMessage, setCurrentMessage] = useState(message);

    const handleSubmit = () => {
        onSave(currentMessage);
    }

    const onChange = (e) => {
        setCurrentMessage ({
            "text": e.target.value,
        });
    };
    return (
        <Paper className="message-input" position="fixed" component="form">
            <InputBase 
                className="message-input-text"
                placeholder="Input message"
                inputProps={{ 'aria-label': 'search google maps' }}
                onChange = {onChange}
            />
            <IconButton 
                className="message-input-button" 
                color="primary" 
                aria-label="directions" 
                onClick={ handleSubmit }>
            <DirectionsIcon />
            </IconButton>
        </Paper>
    );
}

export default MessageInput;