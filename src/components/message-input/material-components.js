import Paper from '@material-ui/core/Paper';
import IconButton from '@material-ui/core/IconButton';
import DirectionsIcon from '@material-ui/icons/Directions';
import InputBase from '@material-ui/core/InputBase';

export { Paper, IconButton, DirectionsIcon, InputBase };