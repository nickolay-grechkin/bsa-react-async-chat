import './styles.css';
import logo from './logo.svg';
const Loader = () => (
      <div className="loader">
        <header className="loader-header">
          <img src={logo} className="loader-logo" alt="logo" />
          <p>
            Load data
          </p>
        </header>
      </div>
);


export default Loader;