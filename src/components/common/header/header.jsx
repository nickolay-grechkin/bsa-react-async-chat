import React, { useEffect } from 'react';
import { useDispatch, useSelector } from "react-redux";
import { Link } from 'react-router-dom';
import dateFormat from 'dateformat';
import { messages as messagesActionCreator } from '../../../store/actions';
import { ChatPath } from  "../../../common/enums/enums";
import Loader from '../loader/loader';
import { AppBar, Toolbar, Badge, MailIcon, GroupIcon, Icon, Typography } from './material-components';
import './style.css';

const Header = () => {
  var messageTime = " ";
  const { messages, status } = useSelector(({ messages }) => ({
    messages: messages.messages,
    status: messages.status,
  }));
  const dispatch = useDispatch();
  useEffect(() => {
    dispatch(messagesActionCreator.fetchMessages());
  }, [dispatch]);

  const usersNames = messages.map(function (message) {
    return message.userIdent;
  })

  if (messages.length !== 0) {
    messageTime = messages[messages.length-1].createdAt;
  }

  if (status === "pending") {
    return <Loader />
  }

  const uniqueUsers = Array.from(new Set(usersNames));
    return (
      <div className="header">
        <AppBar position="fixed">
          <Toolbar>
            <Link className="header-title" to={ChatPath.LOGIN}><Typography variant="h6">BSA CHAT</Typography></Link>
            <Badge badgeContent={messages.length} className="header-messages-count" color="secondary">
              <MailIcon />
            </Badge>
            <Badge badgeContent={uniqueUsers.length} className="header-users-count" color="secondary">
              <GroupIcon />
            </Badge>
            <Badge className="header-last-message-date" color="secondary">
              {dateFormat(messageTime, 'UTC:dd.mm.yyyy HH:MM')}                    
            </Badge>
            <Icon className="fa fa-plus-circle" color="primary" />
              </Toolbar>
            </AppBar>
      </div>
    );
}

export default Header