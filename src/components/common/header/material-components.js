import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import Badge from '@material-ui/core/Badge';
import MailIcon from '@material-ui/icons/Mail';
import GroupIcon from '@material-ui/icons/Group';
import Icon from '@material-ui/core/Icon';
import { Typography } from '@material-ui/core';

export { AppBar, Toolbar, Badge, MailIcon, GroupIcon, Icon, Typography };