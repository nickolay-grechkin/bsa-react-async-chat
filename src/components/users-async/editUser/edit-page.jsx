import TextareaAutosize from '@material-ui/core/TextareaAutosize';
import { useState, useEffect, useCallback } from 'react';
import { EMPTY_USER } from '../common/constants';
import { UserKey } from '../../../common/enums/enums';
import { users as usersActionCreator } from '../../../store/actions';
import { user as userActionCreator } from '../../../store/actions';
import {v4 as uuidv4} from 'uuid';
import { useDispatch, useSelector } from 'react-redux';
import { getLastPath } from '../../../helpers/location/get-last-path/get-last-path';
import { Link } from 'react-router-dom';
import { ChatPath } from '../../../common/enums/enums';
import Loader from '../../common/loader/loader';
import './style.css';

const UserAdd = () => {
  const [currentUser, setCurrentUser] = useState(EMPTY_USER);
  const [update, setUpdate] = useState(false);
  const { user, status } = useSelector(({user}) => ({
    user: user.user,
    status: user.status,
  }));

  const dispatch = useDispatch();

  const handleChange = ({ target }) => {
    const { name, value } = target;
    setCurrentUser({ ...currentUser, [name]: value });
  };

  useEffect(() => {
    const id = getLastPath(window.location.pathname);
    if (id !== "edit-user") {
      dispatch(userActionCreator.fetchUser(id));
      setUpdate(true);
    }
    else {
      setUpdate(false);
    }
  }, [dispatch]);

  const handleOnClick = useCallback((stateUser, currentUser, update) => {
    if (update) {
      currentUser.id = stateUser.id;
      dispatch(usersActionCreator.updateUser(currentUser));
    }
    else {
      currentUser.id = uuidv4();
      dispatch(usersActionCreator.addUser(currentUser));
    }
  }, [dispatch]);

  if (status === "pending") {
    return <Loader />;
  }

  return (
    <div className="edit">
      <span className="logo-text">Modifi users</span>
      <TextareaAutosize
        placeholder="Name"
        className="textInput name"
        name={UserKey.NAME}
        onChange={handleChange}  
      />
      <TextareaAutosize
        placeholder="Surname"
        className="textInput surname"
        name={UserKey.SURNAME}
        onChange={handleChange}  
      />
      <TextareaAutosize
        placeholder="Email"
        className="textInput email"
        name={UserKey.EMAIL}
        onChange={handleChange}  
      />
      <TextareaAutosize
        placeholder="Password"
        className="textInput password"
        name={UserKey.PASSWORD}
        onChange={handleChange}  
      />
      <Link to={ChatPath.USERS} className="link-add">
        <button
          className="btn-add"
          onClick={() => handleOnClick(user, currentUser, update)}
          onChange={handleChange} >
            Add
        </button> 
      </Link>
    </div>
  )
}

export default UserAdd;