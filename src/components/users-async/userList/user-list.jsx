import React from "react";
import User from "../user/user";
import { Link } from 'react-router-dom';
import { ChatPath } from '../../../common/enums/enums';
import './style.css';

const UserList = ({users, onDeleteUser}) => {	
    return (
		<div className="row">
			<div className="col-2">
				<Link to={ChatPath.EDITUSER}>
					<button
						className="btn btn-success"
						style={{ margin: "5px" }}
					>
						Add user
					</button>
				</Link>
				<Link to={ChatPath.MESSAGES}>
					<button
						className="btn btn-success"
						style={{ margin: "5px" }}
					>
						Chat
					</button>
				</Link>
			</div>
			<div className="list-group col-10">
				{
					users.map(user => {
						return (
							<User 
								key={user.id}
								user={user}
								onDeleteUser={onDeleteUser}
							/>
						);
					})
				}
			</div>
		</div>
	);
}

export default UserList;