import { useCallback, useEffect } from "react";
import { useDispatch, useSelector } from "react-redux";
import { users as usersActionCreator } from '../../store/actions';
import UserList from "./userList/user-list";
import Loader from "../common/loader/loader";
import "./style.css";

const Users = () => {
    
    const { users, status } = useSelector(({ users }) => ({
        users: users.users,
        status: users.status,
    }));

    const dispatch = useDispatch();

    const handleUserDelete = useCallback((user) => {
        dispatch(usersActionCreator.deleteUser(user));
    }, [dispatch]);

    useEffect(() => {
        dispatch(usersActionCreator.fetchUsers());
    }, [dispatch]);

    if (status === "pending") {
        return <Loader />;
    }

    return (
        <>
            <UserList 
            users={users} 
            onDeleteUser={handleUserDelete} 
            />
        </>
    )
}

export default Users;