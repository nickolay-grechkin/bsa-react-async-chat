import { Link } from 'react-router-dom';
import { ChatPath } from '../../../common/enums/enums';
import "./style.css";

const User = ({user, onDeleteUser}) => {
    
    const handleUserDelete = () => {
        onDeleteUser(user);
    }
    
    return (
        <div className="container list-group-item">
            <div className="row">
                <div className="col-8">
                    <span className="badge badge-secondary float-left" style={{ fontSize: "2em", margin: "5px" }}>{user.name} {user.surname}</span>
                    <span className="badge badge-info" style={{ fontSize: "2em", margin: "5px" }}>{user.email}</span>
                </div>
                    <div className="col-4 btn-group">
                        <Link to={`${ChatPath.EDITUSER}/${user.id}`}>
                            <button className="btn btn-outline-primary" > Edit </button>
                        </Link>
                        <button className="btn btn-outline-dark" onClick={handleUserDelete}> Delete </button>
                    </div>
            </div>
        </div>
    );
}

export default User;