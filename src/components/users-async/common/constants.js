import { UserKey } from "../../../common/enums/enums";

const EMPTY_USER = {
  [UserKey.ID]:'',
  [UserKey.NAME]:'',
  [UserKey.SURNAME]:'',
  [UserKey.EMAIL]:'',
  [UserKey.PASSWORD]:'',
};

export { EMPTY_USER };