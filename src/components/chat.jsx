import { Route, Switch } from 'react-router-dom';
import { ChatPath } from "../common/enums/enums";
import  Messages from "./messages/messages";
import Header from "./common/header/header";
import EditPage from './editPage/edit-page';
import LoginPage from "./loginPage/login-page";
import Users from './users-async/users';
import UserAdd from './users-async/editUser/edit-page';
import NotFound from './not-found/not-found';

const Chat = () => (
    <>
        <main>
            <Switch>
                <Route path={ChatPath.LOGIN} exact component={LoginPage} />
                <Route path={ChatPath.EDIT_$ID} exact component={EditPage} />
                <Route path={ChatPath.EDITUSER} exact component={UserAdd} />
                <Route path={ChatPath.EDITUSER_$ID} exact component={UserAdd} />
                <Route path={ChatPath.USERS} exact component={Users} />
                <Route path={ChatPath.MESSAGES}>
                    <Header />
                    <Messages />     
                </Route>
                <Route path={ChatPath.ROOT} exact component={LoginPage} />
                <Route path={ChatPath.ANY} exact component={NotFound} />
            </Switch>
        </main>
    </>
);

export default Chat;