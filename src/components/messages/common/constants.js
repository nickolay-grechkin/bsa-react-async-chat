import { MessageKey } from "../../../common/enums/enums";

const EMPTY_MESSAGE = {
    [MessageKey.ID]: '',
    [MessageKey.USERID]: '',
    [MessageKey.AVATAR]: '',
    [MessageKey.USER]: '',
    [MessageKey.TEXT]: '',
    [MessageKey.CREATEDAT]: '',
    [MessageKey.EDITEDAT]: '',
};

export { EMPTY_MESSAGE };