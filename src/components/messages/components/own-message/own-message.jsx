import React from 'react';
import { Typography, CardContent, Avatar, HighlightOffIcon, IconButton, EditIcon } from './material-components';
import { ChatPath } from '../../../../common/enums/enums';
import { Link } from 'react-router-dom';
import dateFormat from 'dateformat';
import './style.css';

const OwnMessage = ({message, onMessageDelete}) => {
    
    const handleMessageDelete = () => {
        onMessageDelete(message);
    }
    
    return (
        <div className="own-message">
            <CardContent >                                   
                <Avatar className="own-message-avatar" color="textSecondary" src={message.avatar}/>
                <IconButton color="secondary" aria-label="delete" className="message-delete" onClick={handleMessageDelete}>                                       
                    <HighlightOffIcon />
                </IconButton> 
                <IconButton color="secondary" aria-label="delete" className="message-edit">                                       
                    <Link to={`${ChatPath.EDIT}/${message.id}`}>
                    <EditIcon />
                    </Link>
                </IconButton> 
                <Typography variant="h5" component="h2" className="message-text">
                    {message.text}
                </Typography>
                <Typography align="right" variant="body2" className="message-time">
                    {dateFormat(message.createdAt, 'UTC: HH:MM')}
                </Typography>  
            </CardContent>
        </div>
    );
};

export default OwnMessage;