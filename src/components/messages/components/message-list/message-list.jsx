import Container from '@material-ui/core/Container';
import Message from '../message/message';
import OwnMessage from '../own-message/own-message';
import './style.css';

const MessageList = ({messages, onMessageDelete, onLikeMessage}) => (
    <div className="message-list">
    {messages.map(message => {
      if (message.userIdent === 'b31b3570-8615-446f-96bd-8f70b516431d') {
          return (
            <Container key={message.id}>
              <OwnMessage 
                message={message}
                onMessageDelete={onMessageDelete} 
              />
            </Container>
          );
      }
      return (
        <Container key={message.id}>
          <Message 
            message={message}
            onLikeMessage={onLikeMessage}
          />
        </Container>
      )
    })}
  </div>
)

export default MessageList;