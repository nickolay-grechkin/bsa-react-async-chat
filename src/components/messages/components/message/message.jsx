import { Typography, CardContent, Avatar, FavoriteIcon, IconButton } from './material-components';
import dateFormat from 'dateformat';
import './style.css';

const Message = ({message, onLikeMessage}) => {
    return (
        <div className="message">
            <CardContent >                                     
                <Avatar className="message-user-avatar" src={message.avatar}/>
                <Typography className="message-user-name" color="textSecondary" gutterBottom>                                       
                    {message.user}
                </Typography> 
                <Typography variant="h5" component="h2" className="message-text">
                    {message.text}
                </Typography>
                <Typography align="right" variant="body2" className="message-time">
                    {dateFormat(message.createdAt, 'UTC: HH:MM')}
                </Typography>
                <IconButton className="message-liked" onClick={() => onLikeMessage(message.id)}>
                    <FavoriteIcon className={message.isLiked ? "message-liked":"message-like"} />
                </IconButton>  
            </CardContent>
        </div>
    );
};

export default Message;