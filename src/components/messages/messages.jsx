import { useEffect } from "react";
import { useDispatch, useSelector } from "react-redux";
import { messages as messagesActionCreator } from '../../store/actions';
import Loader  from '../common/loader/loader';
import { DataStatus } from "../../common/enums/enums";
import { MessageList } from "./components/components";
import MessageInput from "../message-input/message-input";
import { useCallback } from "react";
import { EMPTY_MESSAGE } from "./common/constants";
import {v4 as uuidv4} from 'uuid';

const Messages = () => {
    const { messages, status } = useSelector(({ messages }) => ({
        messages: messages.messages,
        status: messages.status,
    }));
    
    const dispatch = useDispatch();

    const handleMessageSave = useCallback((message) => {
        message.id = uuidv4();
        message.userIdent = "b31b3570-8615-446f-96bd-8f70b516431d";
        message.createdAt = new Date().toISOString();
        dispatch(messagesActionCreator.addMessage(message));
    }, [dispatch]);

    const handleLikeMessage = useCallback((id) => {
        dispatch(messagesActionCreator.likeMessage(id));
    }, [dispatch]);

    const handleMessageDelete = useCallback((message) => {
        dispatch(messagesActionCreator.deleteMessage(message));
    }, [dispatch]);

    useEffect(() => {
        dispatch(messagesActionCreator.fetchMessages());
    }, [dispatch]);

    if (status === DataStatus.PENDING) {
        return <Loader />;
    }

    return (
        <>
            <MessageList 
                messages={messages}
                onMessageDelete={handleMessageDelete}
                onLikeMessage={handleLikeMessage} 
            />
            <MessageInput 
                message={EMPTY_MESSAGE}
                onSave={handleMessageSave}
            />
        </>
    );
}

export default Messages;